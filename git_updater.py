from git import Repo
import shutil

def add_file_to_gitlab_repository(repo_path, file_path, branch_name, commit_message):
    # Initialise le référentiel Git
    repo = Repo(repo_path)

    # Copie du fichier JSON dans le répertoire du référentiel
    shutil.copy(file_path, repo_path)

    # Ajoute le fichier JSON dans la zone de staging
    repo.git.add('.')

    # Effectue un commit avec un message
    commit_message = "Ajout du fichier JSON"
    repo.index.commit(commit_message)

    # Pousse les modifications vers le référentiel distant (GitLab)
    origin = repo.remote(name='origin')
    origin.push(branch_name)

# Chemin vers votre répertoire local
repo_path = r'C:\Users\niang\OneDrive\Documents\EPSI\harvester\mpsr_6.1'

# Chemin vers votre fichier JSON
file_path = 'scan_result.json'

# Nom de la branche
branch_name = 'main'

# Message de commit
commit_message = "Ajout du fichier JSON"

# Appel de la fonction pour ajouter le fichier JSON au dépôt GitLab
add_file_to_gitlab_repository(repo_path, file_path, branch_name, commit_message)
